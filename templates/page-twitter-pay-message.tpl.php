<!DOCTYPE html>

<html>
<?php print $head ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
<body>
    <div id="popup">
	<?php if (!empty($logo)): ?>
	<div  id="logopopup">
		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
	</div>
	<?php endif; ?>
	<?php if ($show_messages) { print $messages; } ?>
	<?php print $content; ?>
    </div>

</body>
</html>
