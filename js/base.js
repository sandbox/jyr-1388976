Drupal.behaviors.twitter_pay = function(context){
	$("#twitter-pay-message-form").submit(function(){
		if ($("#twitter-pay-message-form textarea").val().length > 0){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(data){
					window.opener.location.href = window.opener.location.href; 
					window.close();
				}
			});
		}
		
		return false;		
	});
	
	$("#twitter-pay-message-form textarea").click(function(){
		$(this).val('');
	});
}