<?php

function twitter_pay_admin_settings(){
	$form = array();

	$options = twitter_pay_content_type();

  $form['twitter_pay_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content type for sale'),
    '#options' => $options,
    '#default_value' => variable_get('twitter_pay_content_type', array()),
    '#description' => t('Content type for sale'),
	);
	
  $img_path = drupal_get_path('module', 'twitter_pay') . '/images';
  $results = file_scan_directory($img_path, '.png');
  
  $options = array();
  foreach ($results as $image) {
    $options[$image->basename] = theme('image', $image->filename);
  }
  
  $form['twitter_pay_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select pay button'),
    '#options' => $options,
    '#default_value' => variable_get('twitter_pay_button', 'Pay-with-Twitter-lighter.png'),
  );

  $form['twitter_page_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Authorization page open in'),
    '#options' => array('same page', 'popup'),
    '#default_value' => variable_get('twitter_page_button', '1'),
  );
  
  $form['twitter_via_button'] = array(
    '#type' =>  'textfield',
    '#title' => t('Via'),
		'#size' => 20,
	  '#maxlength' => 30,
    '#default_value' => variable_get('twitter_via_button', '@youruser'),
  );
  return system_settings_form($form);
}

function twitter_pay_post($screen_name, $msg){
  module_load_include('inc', 'twitter');

  $id = twitter_pay_twitter_id($screen_name);
  $account = twitter_account_load($id);
  $twitt = twitter_pay_create_twitt($msg);
	twitter_set_status($account, $twitt);
  
  twitter_pay_generate_cookie();
}

function twitter_pay_twitter_id($screen_name){
	$values = db_fetch_array(db_query("SELECT * FROM {twitter_account} WHERE screen_name = '%s'", $screen_name));
 
 	return $values['twitter_uid'];
}

function twitter_pay_create_twitt($msg){
  $node = node_load($_SESSION['n']);
  unset($_SESSION['n']);
	$short_url = "http://".$_SERVER['HTTP_HOST'] . "/" . $node->path;
  $title = $node->title;
	$youruser = variable_get('twitter_via_button', '@youruser');
  $twitt = $msg . " - ". $title ." " . $short_url. " via " . $youruser;

  return $twitt;
}

function twitter_pay_generate_cookie(){
	$seed = '1#xuv32r95#9-)-vnis';
	$hash = sha1(uniqid($seed . mt_rand(), true));
  $hash = substr($hash, 0, 12);

	$k = $_SESSION['py_key'];
	$p = "pay_".$k;
	setcookie($p, $hash, time() + 15778463, '/');
	$_COOKIE[$p] = $hash;

	unset($_SESSION['py_key']);
	die();
}

function twitter_pay_generate_key(){
	
	$uri = explode("http://".$_SERVER['HTTP_HOST'],referer_uri());
 	$node_url = explode("/",drupal_lookup_path('source', substr($uri[1], 1)));
 	$node = node_load($node_url[1]);
 	$title = "tw_" . truncate_utf8(strtolower($node->title), 5,TRUE);
	$_SESSION['n'] = $node_url[1];

	return $title;
}

function twitter_pay_content_type() {
  $contentTypes = content_types();

  foreach ($contentTypes as $k => $content_type) {
		$options[$k] = $content_type['name'];
  }
  
  return $options;
}
